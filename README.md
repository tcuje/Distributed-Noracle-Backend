Distributed Noracle Backend
===================

[![Build Status](https://jenkins.dbis.rwth-aachen.de/buildStatus/icon?job=Distributed-Noracle-Backend)](https://jenkins.dbis.rwth-aachen.de/job/Distributed-Noracle-Backend/)  

This suite of microservices forms the backend of the Distributed Noracle Project.  
Please try out our app at: [dbis.rwth-aachen.de/noracle/](http://dbis.rwth-aachen.de/noracle/)

---------------
